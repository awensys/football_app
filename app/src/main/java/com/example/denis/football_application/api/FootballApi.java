
package com.example.denis.football_application.api;

import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FootballApi implements Parcelable {

    @SerializedName("count")
    @Expose
    private Integer count;
    @SerializedName("filters")
    @Expose
    private Filters filters;
    @SerializedName("competition")
    @Expose
    private Competition competition;
    @SerializedName("matches")
    @Expose
    private List<Match> matches = null;
    public final static Creator<FootballApi> CREATOR = new Creator<FootballApi>() {


        @SuppressWarnings({
                "unchecked"
        })
        public FootballApi createFromParcel(Parcel in) {
            return new FootballApi(in);
        }

        public FootballApi[] newArray(int size) {
            return (new FootballApi[size]);
        }

    };

    protected FootballApi(Parcel in) {
        this.count = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.filters = ((Filters) in.readValue((Filters.class.getClassLoader())));
        this.competition = ((Competition) in.readValue((Competition.class.getClassLoader())));
        in.readList(this.matches, (com.example.denis.football_application.api.Match.class.getClassLoader()));
    }

    /**
     * No args constructor for use in serialization
     */
    public FootballApi() {
    }

    /**
     * @param matches
     * @param count
     * @param filters
     * @param competition
     */
    public FootballApi(Integer count, Filters filters, Competition competition, List<Match> matches) {
        super();
        this.count = count;
        this.filters = filters;
        this.competition = competition;
        this.matches = matches;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Filters getFilters() {
        return filters;
    }

    public void setFilters(Filters filters) {
        this.filters = filters;
    }

    public Competition getCompetition() {
        return competition;
    }

    public void setCompetition(Competition competition) {
        this.competition = competition;
    }

    public List<Match> getMatches() {
        return matches;
    }

    public void setMatches(List<Match> matches) {
        this.matches = matches;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(count);
        dest.writeValue(filters);
        dest.writeValue(competition);
        dest.writeList(matches);
    }

    public int describeContents() {
        return 0;
    }

}
