
package com.example.denis.football_application.api;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Score implements Parcelable {

    @SerializedName("winner")
    @Expose
    private Object winner;
    @SerializedName("duration")
    @Expose
    private String duration;
    @SerializedName("fullTime")
    @Expose
    private FullTime fullTime;
    @SerializedName("halfTime")
    @Expose
    private HalfTime halfTime;
    @SerializedName("extraTime")
    @Expose
    private ExtraTime extraTime;
    @SerializedName("penalties")
    @Expose
    private Penalties penalties;
    public final static Creator<Score> CREATOR = new Creator<Score>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Score createFromParcel(Parcel in) {
            return new Score(in);
        }

        public Score[] newArray(int size) {
            return (new Score[size]);
        }

    };

    protected Score(Parcel in) {
        this.winner = ((Object) in.readValue((Object.class.getClassLoader())));
        this.duration = ((String) in.readValue((String.class.getClassLoader())));
        this.fullTime = ((FullTime) in.readValue((FullTime.class.getClassLoader())));
        this.halfTime = ((HalfTime) in.readValue((HalfTime.class.getClassLoader())));
        this.extraTime = ((ExtraTime) in.readValue((ExtraTime.class.getClassLoader())));
        this.penalties = ((Penalties) in.readValue((Penalties.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     */
    public Score() {
    }

    /**
     * @param extraTime
     * @param halfTime
     * @param duration
     * @param winner
     * @param penalties
     * @param fullTime
     */
    public Score(Object winner, String duration, FullTime fullTime, HalfTime halfTime, ExtraTime extraTime, Penalties penalties) {
        super();
        this.winner = winner;
        this.duration = duration;
        this.fullTime = fullTime;
        this.halfTime = halfTime;
        this.extraTime = extraTime;
        this.penalties = penalties;
    }

    public Object getWinner() {
        return winner;
    }

    public void setWinner(Object winner) {
        this.winner = winner;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public FullTime getFullTime() {
        return fullTime;
    }

    public void setFullTime(FullTime fullTime) {
        this.fullTime = fullTime;
    }

    public HalfTime getHalfTime() {
        return halfTime;
    }

    public void setHalfTime(HalfTime halfTime) {
        this.halfTime = halfTime;
    }

    public ExtraTime getExtraTime() {
        return extraTime;
    }

    public void setExtraTime(ExtraTime extraTime) {
        this.extraTime = extraTime;
    }

    public Penalties getPenalties() {
        return penalties;
    }

    public void setPenalties(Penalties penalties) {
        this.penalties = penalties;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(winner);
        dest.writeValue(duration);
        dest.writeValue(fullTime);
        dest.writeValue(halfTime);
        dest.writeValue(extraTime);
        dest.writeValue(penalties);
    }

    public int describeContents() {
        return 0;
    }

}
