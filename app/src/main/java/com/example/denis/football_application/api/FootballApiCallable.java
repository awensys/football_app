package com.example.denis.football_application.api;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.PATCH;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface FootballApiCallable {


    @Headers({
            "X-Auth-Token: 4b40b866e9e4425ab8c405557ba9895b"
    })

    @GET("matches")
    Call<FootballApi> callMatches(@Query("dateFrom") String dateFrom, @Query("dateTo") String dateTo);


}
