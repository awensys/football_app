package com.example.denis.football_application.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.denis.football_application.api.MatchesAndScores;
import com.example.denis.football_application.R;

import java.util.ArrayList;

public class CustomAdapter extends ArrayAdapter<MatchesAndScores> {

    private ArrayList<MatchesAndScores> items;

    public CustomAdapter(Context context, int textViewResourceId, ArrayList<MatchesAndScores> items) {
        super(context, textViewResourceId, items);
        this.items = items;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.item, null);
        }
        MatchesAndScores matchesAndScores = items.get(position);
        if (matchesAndScores != null) {
            TextView date = v.findViewById(R.id.item_date);
            TextView teams = v.findViewById(R.id.item_teams);
            TextView score = v.findViewById(R.id.item_score);
            if (date != null) {
                date.setText(matchesAndScores.getMatchday());
            }
            if (teams != null) {
                teams.setText(matchesAndScores.getTeams());
            }
            if (score != null) {
                score.setText(matchesAndScores.getScore());
            }
        }
        return v;
    }

}

