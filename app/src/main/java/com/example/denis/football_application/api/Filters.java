
package com.example.denis.football_application.api;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class Filters implements Parcelable {

    public final static Creator<Filters> CREATOR = new Creator<Filters>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Filters createFromParcel(Parcel in) {
            return new Filters(in);
        }

        public Filters[] newArray(int size) {
            return (new Filters[size]);
        }

    };

    protected Filters(Parcel in) {
    }

    public void writeToParcel(Parcel dest, int flags) {
    }

    public int describeContents() {
        return 0;
    }

}
