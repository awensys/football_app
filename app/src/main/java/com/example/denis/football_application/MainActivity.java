package com.example.denis.football_application;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.example.denis.football_application.adapters.CustomAdapter;
import com.example.denis.football_application.api.FootballApi;
import com.example.denis.football_application.api.FootballApiCallable;
import com.example.denis.football_application.api.Match;
import com.example.denis.football_application.api.MatchesAndScores;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.net.ssl.HttpsURLConnection;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.example.denis.football_application.SplashActivity.DEVICE_NAME;
import static com.example.denis.football_application.SplashActivity.LOCATION_LATITUDE;
import static com.example.denis.football_application.SplashActivity.LOCATION_LONGITUDE;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String TAG = "TAG";

    ArrayList<MatchesAndScores> dataList;
    ListView listView;
    CustomAdapter customAdapter;
    Button btn_info;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        dataList = new ArrayList<>();
        listView = findViewById(R.id.list_item);
        customAdapter = new CustomAdapter(this, R.layout.item, dataList);
        btn_info = findViewById(R.id.button_info);
        btn_info.setOnClickListener(this);

        start();

    }

    private void start() {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, 5);
        Date datePlusFiveDays = c.getTime();
        c.add(Calendar.DATE, -10);
        Date dateMinusFiveDays = c.getTime();
        String date1 = DateFormat.format("yyyy-MM-dd", dateMinusFiveDays).toString();
        String date2 = DateFormat.format("yyyy-MM-dd", datePlusFiveDays).toString();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://api.football-data.org/v2/competitions/PL/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        FootballApiCallable footballApiCallable = retrofit.create(FootballApiCallable.class);
        footballApiCallable.callMatches(date1, date2).enqueue(new Callback<FootballApi>() {
            @Override
            public void onResponse(Call<FootballApi> call, Response<FootballApi> response) {
                try {
                    Log.i(TAG, "onResponse: " + response.code());
                    if (response.code() == HttpsURLConnection.HTTP_OK) {

                        for (Match m : response.body().getMatches()) {

                            dataList.add(new MatchesAndScores(
                                    m.getHomeTeam().getName() + " - " + m.getAwayTeam().getName(),
                                    m.getScore().getFullTime().getHomeTeam() + " - " + m.getScore().getFullTime().getAwayTeam(),
                                    m.getUtcDate()));
                        }
                        listView.setAdapter(customAdapter);
                        customAdapter.notifyDataSetChanged();
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<FootballApi> call, Throwable t) {
                Log.i(TAG, "onFailure: " + t.getMessage());
            }
        });
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(this, ActivityInfo.class);
        if (getIntent().getStringExtra(SplashActivity.DEVICE_NAME) != null) {
            intent.putExtra(DEVICE_NAME, getIntent().getStringExtra(SplashActivity.DEVICE_NAME));
        }
        if (getIntent().getStringExtra(SplashActivity.LOCATION_LATITUDE) != null) {
            intent.putExtra(LOCATION_LATITUDE, getIntent().getStringExtra(SplashActivity.LOCATION_LATITUDE));
        }
        if (getIntent().getStringExtra(SplashActivity.LOCATION_LONGITUDE) != null) {
            intent.putExtra(LOCATION_LONGITUDE, getIntent().getStringExtra(SplashActivity.LOCATION_LONGITUDE));
        }


        startActivity(intent);
    }
}
