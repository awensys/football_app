package com.example.denis.football_application.api;

public class MatchesAndScores {


    private String teams;
    private String score;
    private String matchday;

    public MatchesAndScores(String teams, String score, String matchday) {
        this.teams = teams;
        this.score = score;
        this.matchday = matchday;
    }

    public String getTeams() {
        return teams;
    }

    public void setTeams(String teams) {
        this.teams = teams;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getMatchday() {
        return matchday;
    }

    public void setMatchday(String matchday) {
        this.matchday = matchday;
    }
}
