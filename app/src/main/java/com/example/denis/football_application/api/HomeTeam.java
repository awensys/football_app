
package com.example.denis.football_application.api;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HomeTeam implements Parcelable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    public final static Creator<HomeTeam> CREATOR = new Creator<HomeTeam>() {


        @SuppressWarnings({
                "unchecked"
        })
        public HomeTeam createFromParcel(Parcel in) {
            return new HomeTeam(in);
        }

        public HomeTeam[] newArray(int size) {
            return (new HomeTeam[size]);
        }

    };

    protected HomeTeam(Parcel in) {
        this.id = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.name = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     */
    public HomeTeam() {
    }

    /**
     * @param id
     * @param name
     */
    public HomeTeam(Integer id, String name) {
        super();
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(name);
    }

    public int describeContents() {
        return 0;
    }

}
