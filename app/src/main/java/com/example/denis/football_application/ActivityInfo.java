package com.example.denis.football_application;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import static com.example.denis.football_application.SplashActivity.DEVICE_NAME;
import static com.example.denis.football_application.SplashActivity.LOCATION_LATITUDE;
import static com.example.denis.football_application.SplashActivity.LOCATION_LONGITUDE;

public class ActivityInfo extends AppCompatActivity {

    TextView location_tv;
    TextView device_tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        device_tv = findViewById(R.id.device_name);
        location_tv = findViewById(R.id.device_location);

        if (getIntent().getStringExtra(DEVICE_NAME) != null) {
            device_tv.setText("Device name: " + getIntent().getStringExtra(DEVICE_NAME));
        }
        if (getIntent().getStringExtra(LOCATION_LONGITUDE) != null && getIntent().getStringExtra(LOCATION_LATITUDE) != null) {
            location_tv.setText("Location: " + getIntent().getStringExtra(LOCATION_LATITUDE) + " - " + getIntent().getStringExtra(LOCATION_LONGITUDE));
        }

    }
}
