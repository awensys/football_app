
package com.example.denis.football_application.api;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Penalties implements Parcelable {

    @SerializedName("homeTeam")
    @Expose
    private Object homeTeam;
    @SerializedName("awayTeam")
    @Expose
    private Object awayTeam;
    public final static Creator<Penalties> CREATOR = new Creator<Penalties>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Penalties createFromParcel(Parcel in) {
            return new Penalties(in);
        }

        public Penalties[] newArray(int size) {
            return (new Penalties[size]);
        }

    };

    protected Penalties(Parcel in) {
        this.homeTeam = ((Object) in.readValue((Object.class.getClassLoader())));
        this.awayTeam = ((Object) in.readValue((Object.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     */
    public Penalties() {
    }

    /**
     * @param awayTeam
     * @param homeTeam
     */
    public Penalties(Object homeTeam, Object awayTeam) {
        super();
        this.homeTeam = homeTeam;
        this.awayTeam = awayTeam;
    }

    public Object getHomeTeam() {
        return homeTeam;
    }

    public void setHomeTeam(Object homeTeam) {
        this.homeTeam = homeTeam;
    }

    public Object getAwayTeam() {
        return awayTeam;
    }

    public void setAwayTeam(Object awayTeam) {
        this.awayTeam = awayTeam;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(homeTeam);
        dest.writeValue(awayTeam);
    }

    public int describeContents() {
        return 0;
    }

}
