
package com.example.denis.football_application.api;

import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Match implements Parcelable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("season")
    @Expose
    private Season season;
    @SerializedName("utcDate")
    @Expose
    private String utcDate;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("matchday")
    @Expose
    private Integer matchday;
    @SerializedName("stage")
    @Expose
    private String stage;
    @SerializedName("group")
    @Expose
    private String group;
    @SerializedName("lastUpdated")
    @Expose
    private String lastUpdated;
    @SerializedName("score")
    @Expose
    private Score score;
    @SerializedName("homeTeam")
    @Expose
    private HomeTeam homeTeam;
    @SerializedName("awayTeam")
    @Expose
    private AwayTeam awayTeam;
    @SerializedName("referees")
    @Expose
    private List<Object> referees = null;
    public final static Creator<Match> CREATOR = new Creator<Match>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Match createFromParcel(Parcel in) {
            return new Match(in);
        }

        public Match[] newArray(int size) {
            return (new Match[size]);
        }

    };

    protected Match(Parcel in) {
        this.id = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.season = ((Season) in.readValue((Season.class.getClassLoader())));
        this.utcDate = ((String) in.readValue((String.class.getClassLoader())));
        this.status = ((String) in.readValue((String.class.getClassLoader())));
        this.matchday = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.stage = ((String) in.readValue((String.class.getClassLoader())));
        this.group = ((String) in.readValue((String.class.getClassLoader())));
        this.lastUpdated = ((String) in.readValue((String.class.getClassLoader())));
        this.score = ((Score) in.readValue((Score.class.getClassLoader())));
        this.homeTeam = ((HomeTeam) in.readValue((HomeTeam.class.getClassLoader())));
        this.awayTeam = ((AwayTeam) in.readValue((AwayTeam.class.getClassLoader())));
        in.readList(this.referees, (Object.class.getClassLoader()));
    }

    /**
     * No args constructor for use in serialization
     */
    public Match() {
    }

    /**
     * @param id
     * @param awayTeam
     * @param season
     * @param status
     * @param utcDate
     * @param matchday
     * @param referees
     * @param score
     * @param lastUpdated
     * @param group
     * @param stage
     * @param homeTeam
     */
    public Match(Integer id, Season season, String utcDate, String status, Integer matchday, String stage, String group, String lastUpdated, Score score, HomeTeam homeTeam, AwayTeam awayTeam, List<Object> referees) {
        super();
        this.id = id;
        this.season = season;
        this.utcDate = utcDate;
        this.status = status;
        this.matchday = matchday;
        this.stage = stage;
        this.group = group;
        this.lastUpdated = lastUpdated;
        this.score = score;
        this.homeTeam = homeTeam;
        this.awayTeam = awayTeam;
        this.referees = referees;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Season getSeason() {
        return season;
    }

    public void setSeason(Season season) {
        this.season = season;
    }

    public String getUtcDate() {
        return utcDate;
    }

    public void setUtcDate(String utcDate) {
        this.utcDate = utcDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getMatchday() {
        return matchday;
    }

    public void setMatchday(Integer matchday) {
        this.matchday = matchday;
    }

    public String getStage() {
        return stage;
    }

    public void setStage(String stage) {
        this.stage = stage;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(String lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public Score getScore() {
        return score;
    }

    public void setScore(Score score) {
        this.score = score;
    }

    public HomeTeam getHomeTeam() {
        return homeTeam;
    }

    public void setHomeTeam(HomeTeam homeTeam) {
        this.homeTeam = homeTeam;
    }

    public AwayTeam getAwayTeam() {
        return awayTeam;
    }

    public void setAwayTeam(AwayTeam awayTeam) {
        this.awayTeam = awayTeam;
    }

    public List<Object> getReferees() {
        return referees;
    }

    public void setReferees(List<Object> referees) {
        this.referees = referees;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(season);
        dest.writeValue(utcDate);
        dest.writeValue(status);
        dest.writeValue(matchday);
        dest.writeValue(stage);
        dest.writeValue(group);
        dest.writeValue(lastUpdated);
        dest.writeValue(score);
        dest.writeValue(homeTeam);
        dest.writeValue(awayTeam);
        dest.writeList(referees);
    }

    public int describeContents() {
        return 0;
    }

}
